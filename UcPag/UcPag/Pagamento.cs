﻿using System;
namespace UcPag
{
    public class Pagamento
    {
        private double resultado;
        public static double TaxaJuro()
        {
            return 0.01;
        }

        public double Result
        {
            get { return resultado; }
        }

        public void CalculaJuros(double VInicial, int tempo)
        {
            if (VInicial > 0 && tempo > 0)
            {
                resultado = Math.Round(VInicial * Math.Pow(1 + TaxaJuro(), tempo), 2);

            }
            else
            {
                throw new ArgumentOutOfRangeException("0");
            }

        }
    }
}