using Microsoft.VisualStudio.TestTools.UnitTesting;
using UcPag;
namespace UcPagTests
{
    [TestClass]
    public class UcPagTests
    {
        [TestMethod]
        public void VerificandoTaxajuros()
        {
            var txjuros = Pagamento.TaxaJuro();

            Assert.AreEqual(0.01, txjuros, "Erro");
        }

        [TestMethod]
        public void VerificandoCalculo()
        {
            Pagamento pagamento = new Pagamento();
            pagamento.CalculaJuros(100, 5);

            Assert.AreEqual(105.10, pagamento.Result, "calculo errado");
        }

        [TestMethod]
        public void CalculoJurosIgualZERO()
        {
            Pagamento pagamento = new Pagamento();
            Assert.ThrowsException<System.ArgumentOutOfRangeException>(() => pagamento.CalculaJuros(0, 5));
        }

        [TestMethod]
        public void CalculoJurosComTempoIgualZERO()
        {
            Pagamento pagamento = new Pagamento();
            Assert.ThrowsException<System.ArgumentOutOfRangeException>(() => pagamento.CalculaJuros(100, 0));
        }
    }
}
